# swapi

The Start Wars API

## Generator
Add the generator to your dev dependencies

```yaml
dependencies:
  provider: ^6.0.2
  dio: ^4.0.0
```

## Dio
A powerful Http client for Dart, which supports Interceptors, FormData, Request Cancellation, File Downloading, Timeout etc.

## Provider
State Controller

