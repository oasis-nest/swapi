import 'package:flutter_test/flutter_test.dart';
import 'package:swapi/Controller/page_provider_principal.dart';
import 'package:swapi/Model/page_actor.dart';

void main() {
  final provider = PageProviderPrincipal();

  final ListActor actor = ListActor();
  List<ListActor> list = [];
  actor.name = 'Luke Skywalker';
  actor.height = '1.70';
  actor.mass = '120';
  actor.birth_year = '33BBY';
  actor.gender = 'male';
  actor.homeworld = '';
  list.add(actor);

  String gender = 'male';
  String filter = 'male';

  test('Add list actor', () {
    provider.filterActor(list);
    expect(provider.listActor.length, 1);
  });

  test('Gender type label', () {
    expect(provider.labelTypeGender(gender), 'Masculino');
  });

  test('Get icon by gender type', () {
    expect(provider.iconGender(gender), 'male.png');
  });

  test('Gender type label in the list', () {
    provider.filter = 'male';
    expect(provider.labelFilter(), 'Masculino');
  });

  test('Filter the list by gender', () {
    provider.filterListActor();
    provider.listActorFilter.addAll(
        list.where((element) => element.gender == filter));
    expect(provider.listActorFilter.length, 1);
  });

}
