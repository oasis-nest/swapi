import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:swapi/Controller/page_provider_principal.dart';
import 'package:swapi/Model/page_actor.dart';
import 'package:swapi/Model/page_film.dart';
import 'package:swapi/Util/page_label.dart';

class PageApi {
  Dio dio = Dio();

  /// API REST GET THE LIST OF PEOPLE FROM PAGE ONE
  Future getPeopleSWA(PageProviderPrincipal providerPrincipal,
      VoidCallback? Function(dynamic data)? callback) async {
    const url = PageLabel.urlAPI;
    try {
      final response = await dio.get(url);
      if (response.data == null) return;
      callback!(PageActor.map(response.data));
    } on DioError catch (e) {
      callback!(null);
    }
  }

  /// API REST GET THE LIST OF PEOPLE NEXT PAGE
  Future nextPeopleSWA(
      var urlNext, VoidCallback? Function(dynamic data)? callback) async {
    try {
      final response = await dio.get(urlNext);
      if (response.data == null) return;
      callback!(PageActor.map(response.data));
    } on DioError catch (e) {
      callback!(null);
    }
  }

  /// API REST GET THE LIST FILM ACTOR
  Future getListFilmActor(
      var urlNext, VoidCallback? Function(dynamic data)? callback) async {
    try {
      final response = await dio.get(urlNext);
      if (response.data == null) return;
      callback!(PageFilm.map(response.data));
    } on DioError catch (e) {
      callback!(null);
    }
  }
}
