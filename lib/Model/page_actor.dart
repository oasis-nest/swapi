class PageActor{
  int? _count;
  String? _next;
  String? _previous;
  late List<ListActor> _results;

  PageActor();

  int get count => _count!;

  set count(int value) {
    _count = value;
  }

  String get next => _next!;

  set next(String value) {
    _next = value;
  }

  String get previous => _previous!;

  set previous(String value) {
    _previous = value;
  }

  List<ListActor> get results => _results;

  set results(List<ListActor> value) {
    _results = value;
  }

  @override
  String toString() {
    return 'PageActor{_count: $_count, _next: $_next, _previous: $_previous, _results: $_results}';
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map["count"] = _count;
    map["next"] = _next;
    map["previous"] = _previous;
    return map;
  }

  PageActor.map(dynamic obj) {
    _count = obj["count"];
    _next = obj["next"] ??= '';
    _previous = obj["previous"] ??= '';
    _results = obj['results'] == null
        ? []
        : obj['results'].map<ListActor>((json) => ListActor.map(json)).toList();
  }
}

class ListActor {
  String? _name;
  String? _height;
  String? _mass;
  String? _birth_year;
  String? _gender;
  String? _homeworld;
  late List<String> _films;

  ListActor();

  List<String> get films => _films;

  set films(List<String> value) {
    _films = value;
  }

  String get name => _name!;

  set name(String value) {
    _name = value;
  }

  String get height => _height!;

  set height(String value) {
    _height = value;
  }

  String get mass => _mass!;

  set mass(String value) {
    _mass = value;
  }

  String get birth_year => _birth_year!;

  set birth_year(String value) {
    _birth_year = value;
  }

  String get gender => _gender!;

  set gender(String value) {
    _gender = value;
  }

  String get homeworld => _homeworld!;

  set homeworld(String value) {
    _homeworld = value;
  }

  @override
  String toString() {
    return 'ListActor{_name: $_name, _height: $_height, _mass: $_mass, _birth_year: $_birth_year, _gender: $_gender, _homeworld: $_homeworld}';
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map["name"] = _name;
    map["height"] = _height;
    map["mass"] = _mass;
    map["birth_year"] = _birth_year;
    map["gender"] = _gender;
    map["homeworld"] = _homeworld;
    map["films"] = _films;
    return map;
  }

  ListActor.map(dynamic obj) {
    _name = obj["name"];
    _height = obj["height"];
    _mass = obj["mass"];
    _birth_year = obj["birth_year"];
    _gender = obj["gender"] == 'none' ? 'n/a' : obj["gender"];
    _homeworld = obj["homeworld"];
    _films = List<String>.from(obj["films"].map((x) => x));
  }
}
