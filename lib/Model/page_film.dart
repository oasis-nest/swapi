class PageFilm {
  String? _title;
  int? _episode_id;
  String? _opening_crawl;
  String? _director;
  String? _producer;
  String? _release_date;

  PageFilm();

  String get release_date => _release_date!;

  set release_date(String value) {
    _release_date = value;
  }

  String get producer => _producer!;

  set producer(String value) {
    _producer = value;
  }

  String get director => _director!;

  set director(String value) {
    _director = value;
  }

  String get opening_crawl => _opening_crawl!;

  set opening_crawl(String value) {
    _opening_crawl = value;
  }

  int get episode_id => _episode_id!;

  set episode_id(int value) {
    _episode_id = value;
  }

  String get title => _title!;

  set title(String value) {
    _title = value;
  }

  @override
  String toString() {
    return 'PageFilm{_title: $_title, _episode_id: $_episode_id, _opening_crawl: $_opening_crawl, _director: $_director, _producer: $_producer, _release_date: $_release_date}';
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map["title"] = _title;
    map["episode_id"] = _episode_id;
    map["opening_crawl"] = _opening_crawl;
    map["director"] = _director;
    map["producer"] = _producer;
    map["release_date"] = _release_date;
    return map;
  }

  PageFilm.map(dynamic obj) {
    _title = obj["title"];
    _episode_id = obj["episode_id"];
    _opening_crawl = obj["opening_crawl"];
    _director = obj["director"];
    _producer = obj["producer"];
    _release_date = obj["release_date"];
  }
}
