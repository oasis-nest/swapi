class PageLabel {
  /// Label Global
  static const String nameApp = 'SWAPI';
  static const String nameAutor = '2022 \u00a9 ANGEL GÁLVEZ';
  static const String labelWelcome = 'Bienvenido';
  static const String labelSearch = 'Buscar...';
  static const String labelList = 'Actores';
  static const String labelFilter = 'Filtrar por género';
  static const String labelMale = 'Masculino';
  static const String labelFemale = 'Femenino';
  static const String labelHermaphrodite = 'Hermafrodita';
  static const String labelUnknown = 'Sin género';
  static const String labelLoading = 'Cargando...';
  static const String labelFontTitle = 'semibold';
  static const String labelFontSubtitle = 'medium';
  static const String labelProfile = 'Perfil';
  static const String labelheight = 'Estatura';
  static const String labelMass = 'Peso';
  static const String labelGender = 'Genero';
  static const String labelBirthYear = 'Nacimiento';
  static const String labelEpisode = 'Episodio:';
  static const String labelOpening = 'Sinapsis:';
  static const String labelDirector = 'Director:';
  static const String labelProducer = 'Productor:';
  static const String labelDate = 'Fecha de estreno:';
  static const String labelDowload = 'Ver más';
  static const String labelRetry = 'Reintentar';
  static const String labelMessageError = 'Ha ocurrido un error. Verifica tu conexión de internet '
      'e intentalo de nuevo';

  /// Label Route
  static const String routeSplash = 'ROUTE_SPLASH';
  static const String routePrincipal = 'ROUTE_PRINCIPAL';
  static const String routePerfil = 'ROUTE_PERFIL';

  /// Label URL
  static const String urlAPI = 'https://swapi.dev/api/people';
  static const String urlImage = 'assets/image/';
}
