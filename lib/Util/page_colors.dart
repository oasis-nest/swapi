import 'dart:ui';

class PageColors {
  static Color principalColor = const Color.fromRGBO(100, 65, 165, .9);
  static Color nameApp = const Color.fromRGBO(255, 255, 255, .9);
  static Color titleColor = const Color.fromRGBO(30, 51, 84, .8);
  static Color backgroundColor = const Color.fromRGBO(250, 250, 250, .8);
}
