import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swapi/Controller/page_provider_principal.dart';
import 'package:swapi/Util/page_colors.dart';
import 'package:swapi/Util/page_label.dart';

class PageWidgetGlobal {
  Widget appBar(BuildContext context, String titulo) {
    return AppBar(
      title: Text(
        titulo,
        style: TextStyle(
            fontFamily: PageLabel.labelFontTitle,
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: PageColors.titleColor),
      ),
      leading: IconButton(
        icon: const Icon(
          Icons.arrow_back_ios,
          color: Colors.black54,
          size: 25,
        ),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
      elevation: 0,
      backgroundColor: Colors.white,
    );
  }

  Widget labelTextNameApp(String title, double size) {
    return Text(title,
        style: TextStyle(
            fontFamily: PageLabel.labelFontTitle,
            fontWeight: FontWeight.bold,
            color: PageColors.nameApp,
            fontSize: size));
  }

  Widget labelTextAutor(String title, double size) {
    return Text(title,
        style: TextStyle(
            fontFamily: PageLabel.labelFontTitle,
            fontWeight: FontWeight.bold,
            color: PageColors.nameApp.withOpacity(.5),
            fontSize: size));
  }

  Widget labelTextTitle(String title, double size, bool orientation) {
    return Text(
      title,
      overflow: TextOverflow.ellipsis,
      maxLines: 1,
      style: TextStyle(
          fontFamily: PageLabel.labelFontTitle,
          fontWeight: FontWeight.bold,
          color: PageColors.titleColor,
          fontSize: size),
      textAlign: orientation ? TextAlign.center : TextAlign.left,
    );
  }

  Widget labelTextSubtitle(String title, double size, bool orientation) {
    return Text(
      title,
      style: TextStyle(
        fontFamily: PageLabel.labelFontSubtitle,
        color: PageColors.titleColor.withOpacity(.7),
        fontSize: size,
      ),
      textAlign: orientation ? TextAlign.center : TextAlign.left,
    );
  }

  snackBar(BuildContext context, String mensaje) {
    final snackBar = SnackBar(
        behavior: SnackBarBehavior.fixed,
        duration: const Duration(seconds: 3),
        content: Container(
            child: PageWidgetGlobal().labelTextTitle(mensaje, 14, false)),
        backgroundColor: PageColors.principalColor);
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  activeCircularProgress(
      BuildContext context, PageProviderPrincipal providerPrincipal) {
    return Visibility(
      visible: providerPrincipal.stateCircularProgress,
      child: Center(
        child: Container(
          height: 100,
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              CircularProgressIndicator(
                backgroundColor: Colors.white,
                color: PageColors.principalColor,
              ),
              const SizedBox(
                height: 10.0,
              ),
              PageWidgetGlobal()
                  .labelTextTitle(PageLabel.labelLoading, 14, false)
            ],
          ),
        ),
      ),
    );
  }

  activeMessageError(
      BuildContext context, PageProviderPrincipal providerPrincipal) {
    return Visibility(
      visible: providerPrincipal.stateMessageError,
      child: Center(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              PageWidgetGlobal()
                  .labelTextSubtitle(PageLabel.labelMessageError, 16, true),
              const SizedBox(
                height: 10.0,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  onPrimary: Colors.white,
                  primary: PageColors.principalColor,
                  minimumSize: const Size(200, 36),
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                ),
                onPressed: () {
                  print('prueba >>>>>');
                  providerPrincipal.stateCircularProgress = true;
                  providerPrincipal.stateMessageError = false;
                  providerPrincipal.getSWAP(context);
                },
                child: PageWidgetGlobal()
                    .labelTextNameApp(PageLabel.labelRetry, 14),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget activeLinearProgress(
      BuildContext context, PageProviderPrincipal pageProviderPrincipal) {
    return Visibility(
      visible: pageProviderPrincipal.stateLinearProgress,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          alignment: Alignment.bottomCenter,
          height: 5,
          child: LinearProgressIndicator(
            backgroundColor: PageColors.principalColor.withOpacity(.2),
            color: PageColors.principalColor.withOpacity(.8),
          ),
        ),
      ),
    );
  }
}
