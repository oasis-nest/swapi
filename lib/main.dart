import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:swapi/Controller/page_provider_perfil.dart';
import 'package:swapi/Controller/page_provider_principal.dart';
import 'package:swapi/Util/page_colors.dart';
import 'package:swapi/Util/page_label.dart';
import 'package:swapi/View/page_perfil.dart';
import 'package:swapi/View/page_principal.dart';
import 'package:swapi/View/page_splash.dart';

Future main() async {
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: PageColors.principalColor,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark,
    ),
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => PageProviderPrincipal()),
        ChangeNotifierProvider(create: (_) => PageProviderPerfil())
      ],
      child: MaterialApp(
          navigatorKey: navigatorKey,
          title: PageLabel.nameApp,
          debugShowCheckedModeBanner: false,
          theme: ThemeData.light(),
          initialRoute: PageSplash.route,
          routes: <String, WidgetBuilder>{
            PageSplash.route: (_) => const PageSplash(),
            PagePrincipal.route: (_) => PagePrincipal(),
            PagePerfil.route: (_) => PagePerfil(),
          }),
    );
  }
}
