import 'package:flutter/material.dart';
import 'package:swapi/Util/page_colors.dart';
import 'package:swapi/Util/page_global_widget.dart';
import 'package:swapi/Util/page_label.dart';
import 'package:swapi/View/page_principal.dart';

class PageSplash extends StatefulWidget {
  static String route = PageLabel.routeSplash;

  const PageSplash({Key? key}) : super(key: key);

  @override
  State<PageSplash> createState() => _PageSplashState();
}

class _PageSplashState extends State<PageSplash> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    Future.delayed(const Duration(milliseconds: 2000), () {
      Navigator.pushReplacementNamed(context, PagePrincipal.route);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: PageColors.principalColor,
      body: SafeArea(
        child: Stack(
          children: [
            labelNameApp(),
            labelAutor(),
          ],
        ),
      ),
    );
  }

  Widget labelNameApp() {
    return Center(
      key: const Key("TextName"),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [PageWidgetGlobal().labelTextNameApp(PageLabel.nameApp, 50)],
      ),
    );
  }

  Widget labelAutor() {
    return Container(
      key: const Key('TextNameAutor'),
      margin: const EdgeInsets.only(bottom: 10),
      alignment: Alignment.bottomCenter,
      child: PageWidgetGlobal().labelTextAutor(PageLabel.nameAutor, 12),
    );
  }
}
