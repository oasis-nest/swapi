import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:swapi/Controller/page_provider_principal.dart';
import 'package:swapi/Util/page_colors.dart';
import 'package:swapi/Util/page_global_widget.dart';
import 'package:swapi/Util/page_label.dart';
import 'package:swapi/View/page_perfil.dart';

class PagePrincipal extends StatelessWidget {
  static String route = PageLabel.routePrincipal;

  PagePrincipal({Key? key}) : super(key: key);

  PageProviderPrincipal? pageProviderPrincipal;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: PageColors.principalColor,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    if (pageProviderPrincipal == null) {
      pageProviderPrincipal = Provider.of<PageProviderPrincipal>(context);
      pageProviderPrincipal!.getSWAP(context);
      pageProviderPrincipal!.detectScroll();
    }

    return Scaffold(
      backgroundColor: PageColors.backgroundColor.withOpacity(1),
      body: SafeArea(
        child: Stack(
          children: [
            contImageCube(context),
            Container(
              margin: const EdgeInsets.only(top: 130),
              child: Column(
                children: [
                  Expanded(flex: 1, child: contListActor(context)),
                  Expanded(
                      flex: 0,
                      child: PageWidgetGlobal().activeLinearProgress(
                          context, pageProviderPrincipal!))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget contImageCube(BuildContext context) {
    return SizedBox(
      key: const Key('ContImageCube'),
      height: 170,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.only(
              bottomRight: Radius.circular(10.0),
              bottomLeft: Radius.circular(10.0)),
          color: PageColors.principalColor,
          border: Border.all(
            width: .1,
            color: PageColors.principalColor,
          ),
        ),
        child: GestureDetector(
          onTap: () {
            pageProviderPrincipal!
                .topScroll(pageProviderPrincipal!.scrollController!);
          },
          child: Center(
            child: Container(
              margin: const EdgeInsets.only(bottom: 20),
              child: SizedBox(
                width: double.infinity,
                child: Transform(
                  transform: Matrix4.identity()
                    ..setEntry(3, 2, 0.001)
                    // ..rotateX(0.01 * pageProviderPrincipal!.offset.dy),
                    ..rotateY(-0.01 * pageProviderPrincipal!.offset.dx),
                  alignment: FractionalOffset.center,
                  child: SizedBox(
                      height: 90,
                      width: 90,
                      child: Image.asset('${PageLabel.urlImage}cube.png')),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget contListActor(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
      padding: const EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        border: Border.all(
          width: .3,
          color: PageColors.backgroundColor,
        ),
      ),
      width: double.infinity,
      height: MediaQuery.of(context).size.height,
      child: Stack(
        children: [
          PageWidgetGlobal()
              .activeCircularProgress(context, pageProviderPrincipal!),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 0,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Row(
                        children: [
                          Visibility(
                            visible: pageProviderPrincipal!.listActor.isNotEmpty
                                ? true
                                : false,
                            child: SizedBox(
                              height: 35,
                              child: IconButton(
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  contListFilter(
                                      context, pageProviderPrincipal!);
                                },
                                icon: Image.asset(
                                    '${PageLabel.urlImage}filter.png'),
                              ),
                            ),
                          ),
                          Visibility(
                            visible: pageProviderPrincipal!.filter.isEmpty
                                ? false
                                : true,
                            child: Container(
                              margin: const EdgeInsets.only(top: 3),
                              padding: const EdgeInsets.all(4),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: PageColors.backgroundColor,
                                border: Border.all(
                                  width: 1,
                                  color: PageColors.principalColor,
                                ),
                              ),
                              child: Row(
                                children: [
                                  Container(
                                    child: PageWidgetGlobal().labelTextTitle(
                                        pageProviderPrincipal!.labelFilter(),
                                        14,
                                        false),
                                  ),
                                  SizedBox(
                                    height: 20,
                                    child: IconButton(
                                        padding: EdgeInsets.zero,
                                        onPressed: () {
                                          pageProviderPrincipal!.filter = '';
                                          pageProviderPrincipal!.filterActor(
                                              pageProviderPrincipal!.listActor);
                                        },
                                        iconSize: 20,
                                        color: PageColors.principalColor,
                                        icon: const Icon(Icons.clear)),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 25,
              ),
              Expanded(
                flex: 1,
                child: NotificationListener(
                  onNotification: (value) {
                    if (value is ScrollEndNotification) {
                      pageProviderPrincipal!.resetOffset();
                    }
                    return true;
                  },
                  child: RefreshIndicator(
                    color: PageColors.principalColor,
                    onRefresh: () async {
                      pageProviderPrincipal!.pullRefresh(context);
                    },
                    child: GridView.count(
                        physics: const BouncingScrollPhysics(),
                        crossAxisCount: 3,
                        crossAxisSpacing: 3,
                        mainAxisSpacing: 5,
                        controller: pageProviderPrincipal!.scrollController,
                        children: List.generate(
                            pageProviderPrincipal!.filter.isEmpty
                                ? pageProviderPrincipal!.listActor.length
                                : pageProviderPrincipal!.listActorFilter.length,
                            (index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.of(context)
                                  .pushNamed(PagePerfil.route, arguments: {
                                'actor': pageProviderPrincipal!.filter.isEmpty
                                    ? pageProviderPrincipal!.listActor[index]
                                    : pageProviderPrincipal!
                                        .listActorFilter[index]
                              });
                            },
                            child: Card(
                              elevation: 7,
                              shadowColor:
                                  PageColors.backgroundColor.withOpacity(.8),
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: PageColors.backgroundColor,
                                    width: .1),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Container(
                                alignment: Alignment.bottomCenter,
                                padding: const EdgeInsets.all(3),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: Image.asset(
                                            '${PageLabel.urlImage}'
                                            '${pageProviderPrincipal!.iconGender(pageProviderPrincipal!.filter.isEmpty ? pageProviderPrincipal!.listActor[index].gender : pageProviderPrincipal!.listActorFilter[index].gender)}',
                                            width: 30,
                                            color: Colors.black54)),
                                    PageWidgetGlobal().labelTextTitle(
                                        pageProviderPrincipal!.filter.isEmpty
                                            ? pageProviderPrincipal!
                                                .listActor[index].name
                                            : pageProviderPrincipal!
                                                .listActorFilter[index].name,
                                        12,
                                        true),
                                    PageWidgetGlobal().labelTextSubtitle(
                                        pageProviderPrincipal!.filter.isEmpty
                                            ? pageProviderPrincipal!
                                                .labelTypeGender(
                                                    pageProviderPrincipal!
                                                        .listActor[index]
                                                        .gender)
                                            : pageProviderPrincipal!
                                                .labelTypeGender(
                                                    pageProviderPrincipal!
                                                        .listActorFilter[index]
                                                        .gender),
                                        12,
                                        false),
                                    PageWidgetGlobal().labelTextSubtitle(
                                        '${pageProviderPrincipal!.filter.isEmpty ? pageProviderPrincipal!.listActor[index].films.length : pageProviderPrincipal!.listActorFilter[index].films.length} films',
                                        14,
                                        false),
                                  ],
                                ),
                              ),
                            ),
                          );
                        })),
                  ),
                ),
              ),
            ],
          ),
          PageWidgetGlobal()
              .activeMessageError(context, pageProviderPrincipal!),
        ],
      ),
    );
  }

  Future contListFilter(
      BuildContext context, PageProviderPrincipal pageProviderPrincipal) {
    return showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (context) {
          return Container(
            height: 280,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
              border: Border.all(
                width: .1,
                color: PageColors.principalColor,
              ),
            ),
            margin: const EdgeInsets.all(5),
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                PageWidgetGlobal()
                    .labelTextTitle(PageLabel.labelFilter, 20, true),
                Divider(
                  color: Colors.grey.shade200,
                  thickness: 1.0,
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: PageWidgetGlobal()
                            .labelTextSubtitle(PageLabel.labelMale, 18, false)),
                    Expanded(
                      flex: 0,
                      child: SizedBox(
                          height: 30,
                          child: Checkbox(
                              activeColor: PageColors.principalColor,
                              shape: const CircleBorder(),
                              value: pageProviderPrincipal.filter.isEmpty
                                  ? false
                                  : pageProviderPrincipal.filter == 'male'
                                      ? true
                                      : false,
                              onChanged: (value) {
                                pageProviderPrincipal.filter = 'male';
                                Navigator.of(context).pop();
                                pageProviderPrincipal.filterListActor();
                              })),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: PageWidgetGlobal().labelTextSubtitle(
                            PageLabel.labelFemale, 18, false)),
                    Expanded(
                      flex: 0,
                      child: SizedBox(
                          height: 30,
                          child: Checkbox(
                              activeColor: PageColors.principalColor,
                              shape: const CircleBorder(),
                              value: pageProviderPrincipal.filter.isEmpty
                                  ? false
                                  : pageProviderPrincipal.filter == 'female'
                                      ? true
                                      : false,
                              onChanged: (value) {
                                pageProviderPrincipal.filter = 'female';
                                Navigator.of(context).pop();
                                pageProviderPrincipal.filterListActor();
                              })),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: PageWidgetGlobal().labelTextSubtitle(
                            PageLabel.labelHermaphrodite, 18, false)),
                    Expanded(
                      flex: 0,
                      child: SizedBox(
                          height: 20,
                          child: Checkbox(
                              activeColor: PageColors.principalColor,
                              shape: const CircleBorder(),
                              value: pageProviderPrincipal.filter.isEmpty
                                  ? false
                                  : pageProviderPrincipal.filter ==
                                          'hermaphrodite'
                                      ? true
                                      : false,
                              onChanged: (value) {
                                pageProviderPrincipal.filter = 'hermaphrodite';
                                Navigator.of(context).pop();
                                pageProviderPrincipal.filterListActor();
                              })),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: PageWidgetGlobal().labelTextSubtitle(
                            PageLabel.labelUnknown, 18, false)),
                    Expanded(
                      flex: 0,
                      child: SizedBox(
                          height: 20,
                          child: Checkbox(
                              activeColor: PageColors.principalColor,
                              shape: const CircleBorder(),
                              value: pageProviderPrincipal.filter.isEmpty
                                  ? false
                                  : pageProviderPrincipal.filter == 'n/a'
                                      ? true
                                      : false,
                              onChanged: (value) {
                                pageProviderPrincipal.filter = 'n/a';
                                Navigator.of(context).pop();
                                pageProviderPrincipal.filterListActor();
                              })),
                    ),
                  ],
                )
              ],
            ),
          );
        });
  }
}
