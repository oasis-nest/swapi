import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swapi/Controller/page_provider_perfil.dart';
import 'package:swapi/Model/page_actor.dart';
import 'package:swapi/Model/page_film.dart';
import 'package:swapi/Util/page_colors.dart';
import 'package:swapi/Util/page_global_widget.dart';

import '../Util/page_label.dart';

class PagePerfil extends StatelessWidget {
  static String route = PageLabel.routePerfil;
  PageProviderPerfil? pageProviderPerfil;
  ListActor? actor;

  @override
  Widget build(BuildContext context) {
    dynamic args = ModalRoute.of(context)!.settings.arguments;

    if (pageProviderPerfil == null) {
      pageProviderPerfil = Provider.of<PageProviderPerfil>(context);
      activeProgress(context);
      if (args != null) {
        actor = args['actor'];
        pageProviderPerfil!.addListFilm(context, actor!);
      }
    }

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
          preferredSize: const Size.fromHeight(50),
          child: Visibility(
              visible: pageProviderPerfil!.listFilm.isEmpty ? false : true,
              child:
                  PageWidgetGlobal().appBar(context, PageLabel.labelProfile))),
      body: SafeArea(
          child: Stack(
        children: [
          SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Container(
              color: Colors.white,
              margin: const EdgeInsets.all(3),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(flex: 0, child: contInfoActor()),
                  listaFilm(context)
                ],
              ),
            ),
          )
        ],
      )),
    );
  }

  Widget contInfoActor() {
    return Visibility(
      visible: pageProviderPerfil!.listFilm.isEmpty ? false : true,
      child: Container(
        margin: const EdgeInsets.only(top: 10),
        child: Column(
          children: [
            Expanded(
              flex: 0,
              child: CircleAvatar(
                maxRadius: 40,
                backgroundColor: PageColors.principalColor,
                child: Container(
                    child: PageWidgetGlobal()
                        .labelTextNameApp(actor!.name[0], 50)),
              ),
            ),
            Expanded(
              flex: 0,
              child: Container(
                margin: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    PageWidgetGlobal().labelTextTitle(actor!.name, 30, true),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                PageWidgetGlobal()
                                    .labelTextTitle(actor!.height, 20, false),
                                PageWidgetGlobal().labelTextSubtitle(
                                    PageLabel.labelheight, 16, false),
                              ],
                            )),
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                PageWidgetGlobal()
                                    .labelTextTitle(actor!.mass, 20, false),
                                PageWidgetGlobal().labelTextSubtitle(
                                    PageLabel.labelMass, 16, false),
                              ],
                            )),
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                PageWidgetGlobal().labelTextTitle(
                                    actor!.birth_year, 20, false),
                                PageWidgetGlobal().labelTextSubtitle(
                                    PageLabel.labelBirthYear, 16, false),
                              ],
                            ))
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Divider(
                      color: Colors.grey.shade200,
                      thickness: 1.0,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget listaFilm(BuildContext context) {
    return Visibility(
      visible: pageProviderPerfil!.listFilm.isNotEmpty ? true : false,
      child: Container(
        margin: const EdgeInsets.all(10),
        child: Stack(
          children: [
            ListView.builder(
              shrinkWrap: true,
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: pageProviderPerfil!.listFilm.length,
              itemBuilder: (context, index) {
                return Container(
                  child: itemFilm(
                      context, pageProviderPerfil!.listFilm[index], index),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget itemFilm(BuildContext context, PageFilm listFilm, int index) {
    return Card(
        elevation: 7,
        shadowColor: PageColors.backgroundColor.withOpacity(.8),
        color: Colors.white,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: PageColors.backgroundColor, width: .1),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                padding: const EdgeInsets.all(10),
                child: PageWidgetGlobal()
                    .labelTextTitle(listFilm.title, 25, false)),
            Container(
              padding: const EdgeInsets.all(10),
              child: Row(
                children: [
                  PageWidgetGlobal()
                      .labelTextTitle(PageLabel.labelEpisode, 18, true),
                  const SizedBox(
                    width: 10,
                  ),
                  PageWidgetGlobal()
                      .labelTextSubtitle('${listFilm.episode_id}', 18, false),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PageWidgetGlobal()
                      .labelTextTitle(PageLabel.labelOpening, 18, false),
                  const SizedBox(
                    width: 10,
                  ),
                  PageWidgetGlobal()
                      .labelTextSubtitle(listFilm.opening_crawl, 18, false),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: Row(
                children: [
                  Expanded(
                    flex: 0,
                    child: PageWidgetGlobal()
                        .labelTextTitle(PageLabel.labelDirector, 18, true),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                      flex: 1,
                      child: PageWidgetGlobal()
                          .labelTextSubtitle(listFilm.director, 18, false)),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: Row(
                children: [
                  Expanded(
                    flex: 0,
                    child: PageWidgetGlobal()
                        .labelTextTitle(PageLabel.labelProducer, 18, true),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                      flex: 1,
                      child: PageWidgetGlobal()
                          .labelTextSubtitle(listFilm.producer, 18, false)),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: Row(
                children: [
                  Expanded(
                    flex: 0,
                    child: PageWidgetGlobal()
                        .labelTextTitle(PageLabel.labelDate, 18, true),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    flex: 1,
                    child: PageWidgetGlobal()
                        .labelTextSubtitle(listFilm.release_date, 18, false),
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  activeProgress(BuildContext context) async {
    await Future.delayed(const Duration(milliseconds: 50), () {});
    showDialog(
      barrierColor: Colors.white,
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          child: Container(
            color: Colors.transparent,
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(
                  backgroundColor: Colors.white,
                  color: PageColors.principalColor,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                PageWidgetGlobal()
                    .labelTextTitle(PageLabel.labelLoading, 14, true)
              ],
            ),
          ),
        );
      },
    );
  }
}
