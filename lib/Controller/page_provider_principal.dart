import 'package:flutter/material.dart';
import 'package:swapi/Model/page_actor.dart';
import 'package:swapi/Util/page_label.dart';

import '../Api/page_api.dart';
import '../Util/page_global_widget.dart';

class PageProviderPrincipal extends ChangeNotifier {
  bool? _stateGrid = true;
  PageActor? _pageActor = PageActor();
  late List<ListActor> _listActor = [];
  late List<ListActor> _listFirstActor = [];
  late List<ListActor> _listActorFilter = [];
  bool? _stateCircularProgress = true;
  bool? _stateLinearProgress = false;
  bool? _stateMessageError = false;
  bool? _statePull = false;
  String? _filter = '';
  bool? _stateSearch = false;
  late List<ListActor> _tempList = [];
  late Offset _offset = Offset.zero;
  ScrollController? _scrollController;

  ScrollController? get scrollController => _scrollController;

  set scrollController(ScrollController? value) {
    if (value != null) {
      _scrollController = value;
    }

    notifyListeners();
  }

  Offset get offset => _offset;

  set offset(Offset value) {
    _offset = value;
    notifyListeners();
  }

  List<ListActor> get tempList => _tempList;

  set tempList(List<ListActor> value) {
    _tempList = value;
    notifyListeners();
  }

  bool get stateMessageError => _stateMessageError!;

  set stateMessageError(bool value) {
    _stateMessageError = value;
    notifyListeners();
  }

  bool get stateSearch => _stateSearch!;

  set stateSearch(bool value) {
    _stateSearch = value;
    notifyListeners();
  }

  List<ListActor> get listActorFilter => _listActorFilter;

  set listActorFilter(List<ListActor> value) {
    _listActorFilter = value;
    notifyListeners();
  }

  String get filter => _filter!;

  set filter(String value) {
    _filter = value;
    notifyListeners();
  }

  bool get statePull => _statePull!;

  set statePull(bool value) {
    _statePull = value;
    notifyListeners();
  }

  List<ListActor> get listFirstActor => _listFirstActor;

  set listFirstActor(List<ListActor> value) {
    _listFirstActor = value;
    notifyListeners();
  }

  bool get stateLinearProgress => _stateLinearProgress!;

  set stateLinearProgress(bool value) {
    _stateLinearProgress = value;
    notifyListeners();
  }

  bool get stateCircularProgress => _stateCircularProgress!;

  set stateCircularProgress(bool value) {
    _stateCircularProgress = value;
    notifyListeners();
  }

  bool get stateGrid => _stateGrid!;

  set stateGrid(bool? value) {
    _stateGrid = value;
    notifyListeners();
  }

  PageActor get pageActor => _pageActor!;

  set pageActor(PageActor value) {
    _pageActor = value;
  }

  List<ListActor> get listActor => _listActor;

  set listActor(List<ListActor> value) {
    _listActor = value;
    notifyListeners();
  }

  /// Consult API

  getSWAP(BuildContext context) async {
    await Future.delayed(const Duration(milliseconds: 300), () {});
    PageWidgetGlobal().activeCircularProgress(context, this);
    PageApi().getPeopleSWA(this, (data) {
      if (data == null) {
        stateMessageError = true;
        stateCircularProgress = false;
        notifyListeners();
      } else {
        PageActor actor = data;
        if (actor.results.length < 23 && actor.next.isNotEmpty) {
          if (tempList.isEmpty) {
            tempList.addAll(actor.results);
          }
          getNextSWAP(data.next);
        } else {
          listActor.addAll(actor.results);
        }
      }
    });
    notifyListeners();
  }

  getNextSWAP(String url) {
    if (url.isEmpty) {
      stateLinearProgress = false;
      return;
    }

    PageApi().nextPeopleSWA(url, (data) {
      if (statePull) {
        statePull = false;
      }
      pageActor = data;
      filterActor(data.results);
      return null;
    });
  }

  /// Method

  filterActor(List<ListActor> list) {
    List<ListActor> tempNextList = [];
    tempNextList.addAll(tempList.isNotEmpty ? tempList : listActor);
    tempNextList.addAll(list);
    listActor.clear();
    tempList.clear();

    listActor.addAll(tempNextList.map((item) => item).toSet().toList());

    if (filter.isNotEmpty) {
      if (listFirstActor.isEmpty) {
        listFirstActor.addAll(listActor);
      } else {
        filterListActor();
      }
    }
    _stateMessageError = false;
    _stateCircularProgress = false;
    _stateLinearProgress = false;
    notifyListeners();
  }

  pullRefresh(BuildContext context) async {
    statePull = true;
    notifyListeners();
    getSWAP(context);
  }

  topScroll(ScrollController scrollController) {
    scrollController.animateTo(
      scrollController.position.minScrollExtent,
      duration: const Duration(milliseconds: 500),
      curve: Curves.ease,
    );
  }

  iconGender(String gender) {
    switch (gender) {
      case 'male':
        return 'male.png';
      case 'female':
        return 'female.png';
      case 'hermaphrodite':
        return 'hermaphrodite.png';
      case 'n/a':
        return 'neutral.png';
      case 'none':
        return 'neutral.png';
    }
  }

  labelTypeGender(String gender) {
    switch (gender) {
      case 'male':
        return PageLabel.labelMale;
      case 'female':
        return PageLabel.labelFemale;
      case 'hermaphrodite':
        return PageLabel.labelHermaphrodite;
      case 'n/a':
        return PageLabel.labelUnknown;
      case 'none':
        return PageLabel.labelUnknown;
    }
  }

  labelFilter() {
    switch (filter) {
      case 'male':
        return PageLabel.labelMale;
      case 'female':
        return PageLabel.labelFemale;
      case 'hermaphrodite':
        return PageLabel.labelHermaphrodite;
      case 'n/a':
        return PageLabel.labelUnknown;
      case 'none':
        return PageLabel.labelUnknown;
      case '':
        return '';
    }
  }

  filterListActor() {
    listActorFilter.clear();
    List<ListActor> tempList = listActor.map((item) => item).toSet().toList();
    listActorFilter
        .addAll(tempList.where((element) => element.gender == filter));
    notifyListeners();
  }

  updateOffset(double offsetX, double offSetY) {
    offset += Offset(offsetX, offSetY);
    notifyListeners();
  }

  resetOffset() {
    offset = Offset.zero;
    notifyListeners();
  }

  Future detectScroll() async {
    _scrollController = ScrollController(
      initialScrollOffset: 0.0,
      keepScrollOffset: true,
    );
    _scrollController!.addListener(() {
      if (_scrollController!.position.pixels ==
          _scrollController!.position.maxScrollExtent) {
        stateLinearProgress = true;

        getNextSWAP(pageActor.next);
      }
      if (_scrollController!.position.isScrollingNotifier.value) {
        updateOffset(-2.5, 0.0);
      }
    });
  }
}
