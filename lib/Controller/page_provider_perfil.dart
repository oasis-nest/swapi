import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:swapi/Api/page_api.dart';
import 'package:swapi/Model/page_actor.dart';
import 'package:swapi/Model/page_film.dart';

class PageProviderPerfil extends ChangeNotifier {
  ListActor? _pageActor = ListActor();
  late List<PageFilm> _listFilm = [];

  List<PageFilm> get listFilm => _listFilm;

  set listFilm(List<PageFilm> value) {
    _listFilm = value;
    notifyListeners();
  }

  ListActor? get pageActor => _pageActor;

  set pageActor(ListActor? value) {
    if (value != null) {
      _pageActor = value;
    }

    notifyListeners();
  }

  addListFilm(BuildContext context, ListActor actor) async {
    if (listFilm.isNotEmpty) {
      listFilm.clear();
    }
    await Future.delayed(const Duration(milliseconds: 300), () {});
    if (actor.films.isEmpty) return;

    for (int i = 0; i < actor.films.length; i++) {
      PageApi().getListFilmActor(actor.films[i], (data) {
        listFilm.add(data);
        if (listFilm.length == 1) {
          Navigator.pop(context);
        }
        notifyListeners();
        return null;
      });
    }
  }
}
